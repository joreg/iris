#region usings
using System;
using System.Collections.Generic;
using System.Net;
using System.ComponentModel.Composition;

using VVVV.PluginInterfaces.V1;
using VVVV.PluginInterfaces.V2;
using VVVV.PluginInterfaces.V2.Graph;
using VVVV.Utils.VColor;
using VVVV.Utils.VMath;
using VVVV.Utils.OSC;

using VVVV.Core;
using VVVV.Core.Logging;
#endregion usings

namespace VVVV.Nodes
{
	#region PluginInfo
	[PluginInfo(Name = "GUIExporter", 
				Category = "IRIS", 
				Help = "", Tags = "",
				AutoEvaluate = true)]
	#endregion PluginInfo
	public class NetworkGUIServerNode: IDisposable, IPluginEvaluate
	{
		#region fields & pins
		[Input("FX Path", IsSingle = true)]
		ISpread<string>FFXPath;
		
		[Input("Update GUI", IsBang = true)]
		ISpread<bool> FUpdate;

		[Output("Output")]
		ISpread<string> FOutput;

		[Import()]
		ILogger FLogger;
		
		[Import()]
		IHDEHost FHDEHost;
		
		[Import()]
		IPluginHost FPluginHost;
		
		private INode2 FPatch;
		private bool FFirstFrame = true;
		private Dictionary<string, INode2> FInputs = new Dictionary<string, INode2>();
		
		// Track whether Dispose has been called.
		private bool FDisposed = false;
		#endregion fields & pins
		
		#region constructor/destructor
		public NetworkGUIServerNode()
		{}
		
		~NetworkGUIServerNode()
		{
			Dispose(false);
		}
		
		public void Dispose()
		{
			Dispose(true);
		}
		
		protected void Dispose(bool disposing)
		{
			// Check to see if Dispose has already been called.
			if(!FDisposed)
			{
				if(disposing)
				{
					// Dispose managed resources.
					FPatch.Added -= NodeAddedCB;
					FPatch.Removed -= NodeRemovedCB;
					
					//unregister all ioboxes
					foreach (var node in FPatch)
						if (node.NodeInfo.Filename.Contains(FFXPath[0]))
							RemoveInputs(node);
				}
				// Release unmanaged resources. If disposing is false,
				// only the following code is executed.
			}
			FDisposed = true;
		}
		#endregion constructor/destructor
		
		#region events
		//when a node is removed from the patch 
		//and it is one of the observed subpatches
		//then unregister all parameter-pins
		//reregister all parameter-pins
		
		private void NodeAddedCB(IViewableCollection<INode2> collection, INode2 node)
		{
			if (node.NodeInfo.Filename.Contains(FFXPath[0]))
				AddInputs(node);
			
			FOutput.AssignFrom(FInputs.Keys);
		}
		
		private void NodeRemovedCB(IViewableCollection<INode2> collection, INode2 node)
		{
			RemoveInputs(node);
			
			FOutput.AssignFrom(FInputs.Keys);
		}
		
		private void InputChangedCB(object sender, EventArgs e)
		{
			var patch = (sender as IPin2).ParentNodeByPatch(FPatch);
			RemoveInputs(patch);
			
//			FLogger.Log(LogType.Debug, patch.Name);
			
			AddInputs(patch);
			FOutput.AssignFrom(FInputs.Keys);
		}
		#endregion events
		
		//called when data for any output pin is requested
		public void Evaluate(int SpreadMax)
		{
			if (FFirstFrame)
			{
				string nodePath;
				FPluginHost.GetNodePath(false, out nodePath);
				var node = FHDEHost.GetNodeFromPath(nodePath);
				FPatch = node.Parent;
				
				//register at this nodes patch to be informed of added/removed nodes
				FPatch.Added += NodeAddedCB;
				FPatch.Removed += NodeRemovedCB;
					
				UpdateAllInputs();
				FOutput.AssignFrom(FInputs.Keys);
				FFirstFrame = false;
			}
			
			//update can be forced
			if (FUpdate[0])
			{
				FInputs.Clear();
				UpdateAllInputs();
				FOutput.AssignFrom(FInputs.Keys);
			}

			//FLogger.Log(LogType.Debug, "Logging to Renderer (TTY)");
		}
		
		private void UpdateAllInputs()
		{
			//go through all nodes in this patch
			//if it is a node from a given path
			//extract its inputs
			foreach (var node in FPatch)
				if (node.NodeInfo.Filename.Contains(FFXPath[0]))
					AddInputs(node);
		}
		
		private void AddInputs(INode2 patch)
		{
			//for all nodes in the patch
			foreach (var node in patch)
			{
				//check for ioboxes that are inputs to the patch
				if ((!FInputs.ContainsValue(node))
					&& (node.NodeInfo.Name == "IOBox")
					&& (node.NodeInfo.Category != "Node")
					&& (node.LabelPin.Spread != "||")
					&& (node.LabelPin.Spread != "Evaluate"))
				{
					var inputName = GetIOBoxPinName(node.NodeInfo.Category, true); 
					var outputName = GetIOBoxPinName(node.NodeInfo.Category, false); 
					
					var input = node.FindPin(inputName);
					var output = node.FindPin(outputName);
					
					if (!input.IsConnected() && output.IsConnected())
					{
						var param = node.GetNodePath(false) + "/" + inputName;
						
						node.LabelPin.Changed += InputChangedCB;
						var tag = node.FindPin("Tag");
						tag.Changed += InputChangedCB;
						input.SubtypeChanged += InputChangedCB;
						
						param += "|" + GetSubtypeFromIOBox(node);
						FInputs.Add(param, node);
					}
				}
			}
		}
		
		private void RemoveInputs(INode2 patch)
		{
			//remove all inputs of that node
			var nodePath = patch.GetNodePath(false);
			var keysToDelete = new List<string>();
			foreach (var key in FInputs.Keys)
				if (key.StartsWith(nodePath))
					keysToDelete.Add(key); 
			
			foreach (var key in keysToDelete)
			{
				var node = FInputs[key];
				node.LabelPin.Changed -= InputChangedCB;
				var tag = node.FindPin("Tag");
				tag.Changed -= InputChangedCB;
				var input = node.FindPin(GetIOBoxPinName(node.NodeInfo.Category, true));
				input.SubtypeChanged -= InputChangedCB;
				
				FInputs.Remove(key);
			}
		}
		
		private string GetSubtypeFromIOBox(INode2 iobox)
		{
			var category = iobox.NodeInfo.Category;
			var label = iobox.LabelPin.Spread.Trim('|');
			var guiType = "";
			var _default = "";
			var guiHeight = "1";
			var allowMultiple = "0";
			var enums = "null";
			
			if (category == "Value")
			{
				var valueType = iobox.FindPin("Value Type");
				if (valueType.Spread == "Boolean")
				{
					var behavior = iobox.FindPin("Behavior");
					guiType = behavior.Spread;
				}
				else
				{
					var vectorSize = iobox.FindPin("Vector Size").Spread;

					if (vectorSize == "2")
						guiType = "XY";
					else
						guiType = "X";
				}
				
				_default = iobox.FindPin("Default").Spread;
			}
			else if (category == "String")
			{
				var stringType = iobox.FindPin("String Type").Spread;
				if (stringType == "String")
					guiType = "String";
				
				_default = iobox.FindPin("Default").Spread.Trim('|');
			}
			else if (category == "Color")
			{
				guiType = "Color";
				_default = iobox.FindPin("Color Input").Spread.Trim('|');
			}
			else if (category == "Enumerations")
			{
				guiType = "Enum";
				_default = iobox.FindPin("Input Enum").Spread;
				enums = iobox.FindPin("Tag").Spread.Trim('|');
			}
			
			return label + "|" + guiType + "|" + _default + "|" + guiHeight + "|" + allowMultiple + "|" + enums;
		}

		private string GetIOBoxPinName(string pinType, bool input)
		{
			if (pinType == "String")
			{	if (input)
					return "Input String";
				else
					return "Output String";}
			else if (pinType == "Value")
			{	if (input)
					return "Y Input Value";
				else
					return "Y Output Value";}
			else if (pinType == "Color")
			{	if (input)
					return "Color Input";
				else
					return "Color Output";}
			else if (pinType == "Enumerations")
			{	if (input)
					return "Input Enum";
				else
					return "Output Enum";}
			else //assume node
			{	
				if (input)
					return "Input Node";
				else
					return "Output Node";
			}
		}
	}
}
