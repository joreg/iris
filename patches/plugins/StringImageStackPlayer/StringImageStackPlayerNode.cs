#region usings
using System;
using System.IO;
using System.ComponentModel.Composition;
using System.Collections.Generic;

using VVVV.PluginInterfaces.V1;
using VVVV.PluginInterfaces.V2;
using VVVV.Utils.VColor;
using VVVV.Utils.VMath;

using VVVV.Core.Logging;
#endregion usings

namespace VVVV.Nodes
{
	#region PluginInfo
	[PluginInfo(Name = "ImageStackPlayer", Category = "String", Help = "Basic template with one string in/out", Tags = "")]
	#endregion PluginInfo
	public class StringImageStackNode : IPluginEvaluate
	{
		[Input("Texture Paths", DefaultString = "C:")]
		IDiffSpread<string> FTexturePaths;
		[Input("Playback Time")]
		ISpread<double> FPlaybackTime;
		[Input("Preload N Frames", DefaultValue=30)]
		IDiffSpread<int> FPreloadNFrames;
		[Input("Init", IsSingle=true, IsBang = true)]
		ISpread<bool> FInit;
		
		[Output("BG Frame ID")]
		ISpread<int> FBGFrameID;
		[Output("FG Frame ID")]
		ISpread<int> FFGFrameID;
		[Output("Blend Value BG")]
		ISpread<double> FBlendValueBG;
		[Output("Blend Value FG")]
		ISpread<double> FBlendValueFG;
	 	[Output("Buffer List")]
		ISpread<string> FBufferList;
		[Output("FrameIDFromWholeStack")]
		ISpread<int> FFrameIDFromWholeStack;
		[Output("MS Difference")]
		ISpread<double> FMSDifference;
		[Output("Current Mean")]
		ISpread<double> FCurrentMean;
		[Output("Distance to Last Slice")]
		ISpread<double> FDistanceToLastSlice;
		[Output("Debug")]
		ISpread<string> FDebug;
		[Import()]
		ILogger FLogger;
		//called when data for any output pin is requested
		List<double> FCaptureTime = new List<double>();
		List<string> BufferListUnsorted = new List<string>();	
		List<bool> BufferSlots = new List<bool>();		
		Dictionary<string, int> BufferDict = new Dictionary <string, int>();
		Dictionary<string, int> CurrentBufferDict = new Dictionary <string, int>();
		double ForwardOrBackwardN;
		double MeanAge;
		int IndexOfFarestSpread;
		
				
		int SpreadCount;
		int half_SpreadCount;
		int CurrentFrameID = 0;
	
		double momentbetweenframes;
		int temp1234;
		int temp123;
		string captureTime="";
		string filename="";
		public void initialize()
		{
		
			FCaptureTime.Clear();
			for (int i=Math.Max(0,FCaptureTime.Count-1); i<FTexturePaths.SliceCount; i++)
			{		
			   	filename = Path.GetFileNameWithoutExtension(FTexturePaths[i]);
				string[] parts = filename.Split('_');				
				FCaptureTime.Add(Convert.ToDouble(parts[0]));
			}	
		}
		public int GetEmptySlot(List<bool> OccupiedOrEmpty )
		{
			if (OccupiedOrEmpty.Count>0)			
 			{
				for (int i=0; i<OccupiedOrEmpty.Count; i++)
				{
					if (OccupiedOrEmpty[i]==false)
					{		
						temp1234 = i;
						OccupiedOrEmpty[i] = true;
						break;
					}									
				}					
			}
		return temp1234;
		}	
	public int FindClosestNumber(List<double> ArrayToCheck, double MyValue )
		{
	//		temp123 = Math.Abs(ArrayToCheck.BinarySearch(MyValue));
			if (ArrayToCheck.Count>0)
			for (int i=0; i<ArrayToCheck.Count; i++)
			{
				if (ArrayToCheck[i]>MyValue)
				{	
					temp123=i-1;
					break;						
				}
			}
		//	if (Math.Abs(ArrayToCheck[temp123]-MyValue)>Math.Abs(ArrayToCheck[temp123-1]-MyValue))
			

		return Math.Max(0,temp123);		
		}	

	public double GetMean(List<double> MyArray)
	{
		List<double>difference = new List<double>();
		for (int i=1; i<MyArray.Count; i++)
		{
			difference.Add(MyArray[i]-MyArray[i-1]);	
		}
		double mean = 0;
		foreach (double average in difference)
		{
			mean += average;
		}
		mean /= difference.Count;
		return mean;
	}
	public void GetDistanceToLastSlice()
	{
		FDistanceToLastSlice[0]=FCaptureTime[FCaptureTime.Count-1]-FPlaybackTime[0];
	}
		
		public void CreateBufferList()
		{
		//--------------------preparing/clearing--------------	
	
			BufferListUnsorted.Clear();
			BufferSlots.Clear();
			BufferDict.Clear();
			CurrentBufferDict.Clear();
				
		//------------- getting Frame ID from whole spread------------------

			CurrentFrameID = FindClosestNumber(FCaptureTime, FPlaybackTime[0]);
			FFrameIDFromWholeStack[0] = CurrentFrameID;
			if (CurrentFrameID<FCaptureTime.Count-1)
				momentbetweenframes =(Math.Abs(FPlaybackTime[0]-FCaptureTime[CurrentFrameID]))/(FCaptureTime[CurrentFrameID+1]-FCaptureTime[CurrentFrameID]);
			FBlendValueBG[0] = 1;
			FBlendValueFG[0] = momentbetweenframes;
	
			//CurrentBufferDict.Add(FTexturePaths[CurrentFrameID],0);		
			//-------------------init/assigning----------------------
			for (int i=Math.Max(0,Math.Max(0,CurrentFrameID-half_SpreadCount)); i<Math.Min(FTexturePaths.SliceCount, CurrentFrameID+half_SpreadCount); i++)
			{
					BufferListUnsorted.Add(FTexturePaths[i]);
			}
			for (int i=0 ; i<BufferListUnsorted.Count; i++)
			{
				BufferDict.Add(BufferListUnsorted[i],i);
			}
			for (int i=0; i<FBufferList.SliceCount; i++)
			{
				if (CurrentBufferDict.ContainsKey(FBufferList[i])==false)
				CurrentBufferDict.Add(FBufferList[i],i);
			}
			
			
//			if (FTexturePaths.IsChanged)
//		     {
//				FBufferList.AssignFrom(BufferListUnsorted);
//		     }
		//--------------------------resorting bufferlist-------------
			for (int i=0; i<FBufferList.SliceCount; i++)
				{
					if (BufferDict.ContainsKey(FBufferList[i]))
						BufferSlots.Add(true);
					else
						BufferSlots.Add(false);				
				}
			for (int i=0; i<BufferListUnsorted.Count; i++)
			{
				if (CurrentBufferDict.ContainsKey(BufferListUnsorted[i])==false)
					{
						FBufferList[GetEmptySlot(BufferSlots)]=BufferListUnsorted[i];				
					}
	
			}
			//---------getting the indexes from the bufferlist-----------
			FDebug[0]="false";
			for (int i=0; i<FBufferList.SliceCount; i++)	
			{	
				if (FBufferList[i] == FTexturePaths[CurrentFrameID])
					{
						FBGFrameID[0] = i;
						FDebug[0]="true";	
					}
				if (FBufferList[i] == FTexturePaths[CurrentFrameID+1])
						FFGFrameID[0] = i;			
			}	
		}
		
			
		
			//-----------------------------main------------
		public void Evaluate(int SpreadMax)
		{
			FBGFrameID.SliceCount = 1;
			FFGFrameID.SliceCount = 1;
			FBlendValueBG.SliceCount = 1;
			FBlendValueFG.SliceCount = 1;
			FFrameIDFromWholeStack.SliceCount=1;
			FBufferList.SliceCount = SpreadCount;//(Math.Min(FTexturePaths.SliceCount, CurrentFrameID+half_SpreadCount))-(Math.Max(0,Math.Max(0,CurrentFrameID-half_SpreadCount)));//SpreadCount;
			FCurrentMean.SliceCount=1;
			FDistanceToLastSlice.SliceCount=1;
			
			if (FTexturePaths.SliceCount>0)
			{
			//	if (FTexturePaths.IsChanged)
					initialize();
				if ((FInit[0]) || (FPreloadNFrames.IsChanged))
				{
					FCaptureTime.Clear();
					SpreadCount = FPreloadNFrames[0];
					half_SpreadCount = Convert.ToInt16(FPreloadNFrames[0]*0.5);
					initialize();
					CreateBufferList();
					FBufferList.AssignFrom(BufferListUnsorted);
					FCurrentMean[0]=GetMean(FCaptureTime);				
				}
				GetDistanceToLastSlice();
				CreateBufferList();
			
				FMSDifference[0] = FPlaybackTime[0]-ForwardOrBackwardN;
				ForwardOrBackwardN = FPlaybackTime[0];
			}
		
		
			
			}
		}
	
}
