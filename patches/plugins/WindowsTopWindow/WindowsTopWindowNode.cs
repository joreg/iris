#region usings
using System;
using System.ComponentModel.Composition;
using System.Runtime.InteropServices;
using System.Text;

using VVVV.PluginInterfaces.V1;
using VVVV.PluginInterfaces.V2;
using VVVV.Utils.VColor;
using VVVV.Utils.VMath;

using VVVV.Core.Logging;

#endregion usings

namespace VVVV.Nodes
{
	#region PluginInfo
	[PluginInfo(Name = "TopWindow", Category = "Windows", Help = "Get Foreground window name and handle", Tags = "",Author="vux")]
	#endregion PluginInfo
	public class WindowsTopWindowNode : IPluginEvaluate
	{
		[DllImport("user32.dll")]
		private static extern IntPtr GetForegroundWindow();
		
		[DllImport("user32.dll", SetLastError=true, CharSet=CharSet.Auto)]
		static extern int GetWindowTextLength(IntPtr hWnd);
		
		[DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
		static extern int GetWindowText(IntPtr hWnd, StringBuilder lpString, int nMaxCount);
		
		[Input("Refresh", IsBang=true,IsSingle=true,DefaultValue=1)]
		ISpread<bool> FInput;
		
		[Output("Handle", IsSingle=true)]
		ISpread<int> FOutHandle;

		[Output("Window Name", IsSingle=true)]
		ISpread<string> FOutName;

		bool FFirstFrame = true;

		//called when data for any output pin is requested
		public void Evaluate(int SpreadMax)
		{
			if (this.FFirstFrame || FInput[0])
			{
				IntPtr ptr = GetForegroundWindow();
				this.FOutHandle[0] = ptr.ToInt32();
				
				int length = GetWindowTextLength(ptr);
				StringBuilder sb = new StringBuilder(length + 1);
    			GetWindowText(ptr, sb, sb.Capacity);
    			this.FOutName[0] = sb.ToString();
				
			}
			this.FFirstFrame = false;
		}
	}
}
