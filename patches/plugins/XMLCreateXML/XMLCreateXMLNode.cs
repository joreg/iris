#region usings
using System;
//using System.Collections.Generic;
//using Microsoft.CSharp;
//using System.Linq;
using System.ComponentModel.Composition;
//using Microsoft.CSharp.RuntimeBinder;
//using System.Xml;
using System.Xml.Linq;
using VVVV.PluginInterfaces.V1;
using VVVV.PluginInterfaces.V2;
//using VVVV.Utils.VColor;
//using VVVV.Utils.VMath;
//using AmazedSaint.Elastic;
//using AmazedSaint;
using VVVV.Core.Logging;
using AmazedSaint.Elastic.Lib;
#endregion usings

namespace VVVV.Nodes
{
	#region PluginInfo
	[PluginInfo(Name = "CreateXML", Category = "XML", Help = "Basic template with one string in/out", Tags = "")]
	#endregion PluginInfo
	public class XMLCreateXMLNode : IPluginEvaluate
	{

		[Input("IRIS V4Bundle Path", DefaultString = "")]
		ISpread<string> FV4BundlePath;

		[Input("IRIS OSC Channel")]
		ISpread<ISpread<string>> FIRISOSCChannels;

		[Input("IRIS Value Values")]
		ISpread<ISpread<double>> FValueValues;

		[Input("IRIS String Values")]
		ISpread<ISpread<string>> FStringValues;

		[Input("Is String")]
		ISpread<bool> FIsString;

		[Input("Enable", IsBang = true, IsSingle = true)]
		ISpread<bool> FEnable;

		[Output("Output")]
		ISpread<string> FOutput;

		[Output("Debug")]
		ISpread<string> FDebug;

		[Import()]
		ILogger FLogger;


		string[] SlotName = {
			"A1",
			"A2",
			"B1",
			"B2",
			"MIX",
			"OUT"
		};
		//called when data for any output pin is requested




		public string CreateStringValueLine(ISpread<ISpread<string>> g, int f)
		{
			string temp = "";

			for (int j = 0; j < g[f].SliceCount; j++) {
				temp += g[f][j];
				if (j < g[f].SliceCount - 1)
					temp += ";";
			}
			return temp;
		}
		public string CreateValueValueLine(ISpread<ISpread<double>> g, int f)
		{
			string temp = "";

			for (int j = 0; j < g[f].SliceCount; j++) {
				temp += String.Format(System.Globalization.CultureInfo.CreateSpecificCulture("en-US"), "{0:0.0000000}", g[f][j]);
				if (j < g[f].SliceCount - 1)
					temp += ";";
			}
			return temp;
		}
//see source: http://www.amazedsaint.com/2010/02/introducing-elasticobject-for-net-40.html
		public void Evaluate(int SpreadMax)
		{
			if (FEnable[0]) {
				int counterIsString = 0;
				int counter = 0;
				dynamic iris = new ElasticObject("IRIS");
				for (int i = 0; i < FV4BundlePath.SliceCount; i++) {
					var Slot = iris << "NODE";
					Slot.nodename = FV4BundlePath[i];
					Slot.DescriptiveName = SlotName[i];

//				var Path = Slot << "Path";
//				Slot.Path= FPatchesPath[i];
					for (int j = 0; j < FIRISOSCChannels[i].SliceCount; j++) {

						var Pin = Slot << "PIN";
						Pin.pinname = FIRISOSCChannels[i][j];
						if (FIsString[counter]) {
							Pin.values = CreateStringValueLine(FStringValues, counterIsString);
							counterIsString++;
						} else
							Pin.values = CreateValueValueLine(FValueValues, counter);

						counter++;
					}
				}
				XElement el = iris > FormatType.Xml;
				FOutput.SliceCount = 1;
				FOutput[0] = Convert.ToString(el);
				//FLogger.Log(LogType.Debug, "Logging to Renderer (TTY)");

			}

		}
	}
}
