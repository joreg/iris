//@author: vvvv group
//@help: draws a mesh with a constant color
//@tags: template, basic
//@credits:

// --------------------------------------------------------------------------------------------------
// PARAMETERS:
// --------------------------------------------------------------------------------------------------

//transforms
float4x4 tW: WORLD;        //the models world matrix
float4x4 tV: VIEW;         //view matrix as set via Renderer (EX9)
float4x4 tP: PROJECTION;   //projection matrix as set via Renderer (EX9)
float4x4 tWVP: WORLDVIEWPROJECTION;

//material properties
float4 cAmb : COLOR <String uiname="Color";>  = {1, 1, 1, 1};
float Alpha <float uimin=0.0; float uimax=1.0;> = 1;
float Blend  <float uimin=0.0; float uimax=1.0;> = 1;;
//texture
texture TexA <string uiname="Texture A";>;
sampler SampA = sampler_state    //sampler for doing the texture-lookup
{
    Texture   = (TexA);          //apply a texture to the sampler
    MipFilter = LINEAR;         //sampler states
    MinFilter = LINEAR;
    MagFilter = LINEAR;
	AddressU = border;
	AddressV = border;
	BorderColor = {0,0,0,0};
};

texture TexB <string uiname="Texture B";>;
sampler SampB = sampler_state    //sampler for doing the texture-lookup
{
    Texture   = (TexB);          //apply a texture to the sampler
    MipFilter = LINEAR;         //sampler states
    MinFilter = LINEAR;
    MagFilter = LINEAR;
	AddressU = border;
	AddressV = border;
	BorderColor = {0,0,0,0};
};
float4x4 tTexA: TEXTUREMATRIX <string uiname="Texture A Transform";>;
float4x4 tTexB: TEXTUREMATRIX <string uiname="Texture B Transform";>;

//the data structure: vertexshader to pixelshader
//used as output data with the VS function
//and as input data with the PS function
struct vs2ps
{
    float4 Pos : POSITION;
    float4 TexCdA : TEXCOORD0;
	 float4 TexCdB : TEXCOORD1;
};

// --------------------------------------------------------------------------------------------------
// VERTEXSHADERS
// --------------------------------------------------------------------------------------------------

vs2ps VS(
    float4 Pos : POSITION,
    float4 TexCdA : TEXCOORD0,
	float4 TexCdB : TEXCOORD0)
{
    //inititalize all fields of output struct with 0
    vs2ps Out = (vs2ps)0;

    //transform position
    Out.Pos = mul(Pos, tWVP);

    //transform texturecoordinates
    Out.TexCdA = mul(TexCdA, tTexA);
	Out.TexCdB = mul(TexCdB, tTexB);
    return Out;
}

// --------------------------------------------------------------------------------------------------
// PIXELSHADERS:
// --------------------------------------------------------------------------------------------------

float4 PS(vs2ps In): COLOR
{
    //In.TexCd = In.TexCd / In.TexCd.w; // for perpective texture projections (e.g. shadow maps) ps_2_0

    float4 col = (tex2D(SampA, In.TexCdA)* Blend) + (tex2D(SampB, In.TexCdB)*(1-Blend)) * cAmb;
    col.a *= Alpha;
    return col;
}

// --------------------------------------------------------------------------------------------------
// TECHNIQUES:
// --------------------------------------------------------------------------------------------------

technique TConstant
{
    pass P0
    {
        //Wrap0 = U;  // useful when mesh is round like a sphere
        VertexShader = compile vs_2_0 VS();
        PixelShader = compile ps_2_0 PS();
    }
}

technique TConstantFF
{
    pass P0
    {
        //transformations
        WorldTransform[0]   = (tW);
        ViewTransform       = (tV);
        ProjectionTransform = (tP);

        //material
        MaterialAmbient  = {1, 1, 1, 1};
        MaterialDiffuse  = {1, 1, 1, 1};

        //texturing
        Sampler[0] = (SampA);
        TextureTransform[0] = (tTexA);
        TexCoordIndex[0] = 0;
        TextureTransformFlags[0] = COUNT4 | PROJECTED;
        //Wrap0 = U;  // useful when mesh is round like a sphere

        //lighting
        LightEnable[0] = TRUE;
        Lighting       = TRUE;
        SpecularEnable = FALSE;

        LightType[0]     = DIRECTIONAL;
        LightAmbient[0]  = (cAmb);
        LightDiffuse[0]  = {0, 0, 0, 0};
        LightDirection[0] = {0, 0, 1, 1};

        //shading
        ShadeMode = FLAT;
        VertexShader = NULL;
        PixelShader  = NULL;
    }
}